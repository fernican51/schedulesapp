<?php
/**
 * Created by PhpStorm.
 * User: fernandofigueroa
 * Date: 4/28/16
 * Time: 9:38 AM
 */

namespace AppBundle\Util;


use AppBundle\Entity\Notification;
use AppBundle\Entity\User;

class Util
{
    const VALID_EMAIL_DOMAIN = 'admios-sa.com';

    public static function validEmailDomain($email) {
        $start = strrpos($email, "@");
        if($start !== false) {
            $domain = substr($email, $start + 1, strlen($email));
            return $domain === self::VALID_EMAIL_DOMAIN;
        }

        return false;
    }

    public static function getDefaultNotification() {
        $notification = new Notification();
        $notification->setName('Notification Type Deleted');
        $notification->setShortDescription('NTD');
        return $notification;
    }

    public static function getDefaultUser() {
        $user = new User();
        $user->setUsername('deletedUser');
        $user->setEmail('invalid@user.com');
        return $user;
    }

    public static function isTodayWeekend() {
        $weekDay = date('w', strtotime('now'));
        return ($weekDay == 0 || $weekDay == 6);
    }
}