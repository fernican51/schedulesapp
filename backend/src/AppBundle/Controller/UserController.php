<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\ProfileType;
use AppBundle\Form\RegistrationType;
use AppBundle\Util\Util;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class UserController extends FOSRestController
{
    /**
     * Validates and saves the task
     *
     * @param  UserInterface $user
     * @param  Request $request
     * @param  bool $new is new object
     * @return \Symfony\Component\HttpFoundation\Response
     */

    private function processForm(UserInterface $user, Request $request, $new = false)
    {
        $statusCode = $new ? Response::HTTP_CREATED : Response::HTTP_OK;
        $formType = $new ? RegistrationType::class : ProfileType::class;
        $form = $this->createForm($formType, $user);
        $data = $request->request->all();
        $children = $form->all();
        $toBind = array_intersect_key($data, $children);
        $form->submit($toBind);
        if ($form->isValid() && Util::validEmailDomain($user->getEmail())) {
            $returnObject = null;
            if($new)
            {
                $user->setEnabled(true);
                $this->get('fos_user.user_manager')->updateUser($user);
                $returnObject = $user;
            }
            else
            {
                $this->get('fos_user.user_manager')->updateUser($user);
                $auth_service = $this->get('lexik_jwt_authentication.jwt_manager');
                $token_ = $auth_service->create($user);
                $returnObject = array(
                    'user' => $user,
                    'token' => $token_
                );
            }
            return $this->handleView($this->view($returnObject, $statusCode));
        }
        return $this->handleView($this->view($form, Response::HTTP_BAD_REQUEST));
    }
    
    /**
     * @Rest\Get("user")
     * @return \Symfony\Component\HttpFoundation\Response
     * @ApiDoc(
     *  description="Return the users list.",
     *  section="User",
     *  statusCodes={
     *         200="Returned when successful",
     *         500="Returned when a non expected error happened getting the list"
     *     }
     * )
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function getUserListAction()
    {
        $users = $this->getDoctrine()->getRepository('AppBundle:User')->findAll();

        return $this->handleView($this->view(array('list' => $users)));
    }

    /**
     * @Rest\Post("register")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @ApiDoc(
     *  description="Register a user.",
     *  section="User",
     *  statusCodes={
     *         201="Returned when successful",
     *         400="Returned when error exists on the data sent"
     *     }
     * )
     */
    public function postUserAction(Request $request)
    {
        return $this->processForm($this->get('fos_user.user_manager')->createUser(), $request, true);
    }

    /**
     * @Rest\Put("user")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @ApiDoc(
     *  description="Updates user info.",
     *  section="User",
     *  statusCodes={
     *         204="Returned when successful",
     *         400="Returned when error exists on the data sent"
     *     }
     * )
     */
    public function putUserAction(Request $request)
    {
        return $this->processForm($this->getUser(), $request);
    }

    /**
     * @Rest\Delete("user/{id}")
     * @param User $user
     * @return \Symfony\Component\HttpFoundation\Response
     * @ApiDoc(
     *  description="Deletes a user.",
     *  section="User",
     *  statusCodes={
     *         204="Returned when successful",
     *         500="Returned when an unexpected error ocurred"
     *     }
     * )
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function deleteUserAction(User $user)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();
        return $this->handleView($this->view(null, Response::HTTP_NO_CONTENT));
    }
}
