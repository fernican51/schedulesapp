<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Notification;
use AppBundle\Form\NotificationType;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class NotificationController extends FOSRestController
{
    /**
     * Validates and saves the task
     *
     * @param  Notification $notification
     * @param  Request $request
     * @param  bool $new is new object
     * @return \Symfony\Component\HttpFoundation\Response
     */
    private function processForm(Notification $notification, Request $request, $new = false)
    {
        $statusCode = $new ? Response::HTTP_CREATED : Response::HTTP_NO_CONTENT;
        $form = $this->createForm(NotificationType::class, $notification);
        $data = $request->request->all();
        $children = $form->all();
        $toBind = array_intersect_key($data, $children);
        $form->submit($toBind);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($notification);
            $em->flush();
            return $this->handleView($this->view($new ? $notification : null, $statusCode));
        }
        return $this->handleView($this->view($form, Response::HTTP_BAD_REQUEST));
    }

    /**
     * @Rest\Get("notification")
     * @return \Symfony\Component\HttpFoundation\Response
     * @ApiDoc(
     *  description="Return the notification list.",
     *  section="Notification",
     *  statusCodes={
     *         200="Returned when successful",
     *         500="Returned when a non expected error happened getting the list"
     *     }
     * )
     */
    public function getNotificationListAction()
    {
        $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findAll();

        return $this->handleView($this->view(array('list' => $notifications)));
    }

    /**
     * @Rest\Get("notification/{id}")
     * @return \Symfony\Component\HttpFoundation\Response
     * @param Notification $notification
     * @ApiDoc(
     *  description="Return a notification by it's id.",
     *  section="Notification",
     *  statusCodes={
     *         200="Returned when successful",
     *         500="Returned when a non expected error happened getting the notification"
     *     }
     * )
     */
    public function getNotificationAction(Notification $notification)
    {
        return $this->handleView($this->view(array('notification' => $notification)));
    }

    /**
     * @Rest\Delete("notification/{id}")
     * @return \Symfony\Component\HttpFoundation\Response
     * @param Notification $notification
     * @ApiDoc(
     *  description="Deletes a notification.",
     *  section="Notification",
     *  statusCodes={
     *         200="Returned when successful",
     *         500="Returned when a non expected error happened getting the notification"
     *     }
     * )
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function deleteNotificationAction(Notification $notification)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($notification);
        $em->flush();
        return $this->handleView($this->view(null, Response::HTTP_NO_CONTENT));
    }

    /**
     * @Rest\Post("notification")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @ApiDoc(
     *  description="Creates a notification type.",
     *  section="Notification",
     *  statusCodes={
     *         201="Returned when an item is created successfully",
     *         400="Returned when there is invalid data sent"
     *     },
     *  parameters={
     *      {"name"="name", "dataType"="string", "required"=true, "description"="The notification's name"},
     *      {"name"="shortDescription", "dataType"="string", "required"=true, "description"="The notification's short name"}
     *  }
     * )
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function postNotificationAction(Request $request)
    {
        return $this->processForm(new Notification(), $request, true);
    }

    /**
     * @Rest\Put("notification/{id}")
     * @param Notification $notification
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @ApiDoc(
     *  description="Updates a notification type.",
     *  section="Notification",
     *  statusCodes={
     *         204="Returned when an item is updated successfully",
     *         400="Returned when there is invalid data sent"
     *     },
     *  parameters={
     *      {"name"="name", "dataType"="string", "required"=true, "description"="The notification's name"},
     *      {"name"="shortDescription", "dataType"="string", "required"=true, "description"="The notification's short name"}
     *  }
     * )
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function putNotificationAction(Notification $notification, Request $request)
    {
        return $this->processForm($notification, $request);
    }
}
