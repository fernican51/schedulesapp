<?php

namespace AppBundle\Controller;

use AppBundle\Command\EmailReminderCommand;
use AppBundle\Command\SendScheduleCommand;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\CssSelector\Exception\InternalErrorException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\Process\Process;

class TaskController extends FOSRestController
{
    /**
     * @Rest\Post("remainder")
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws InternalErrorException
     * @ApiDoc(
     *  description="Updates the emails that are going to receive the schedule email.",
     *  section="Task",
     *  statusCodes={
     *         200="Returned when the emails are updated successfully",
     *         500="Returned when there was an error executing the command"
     *     },
     *  parameters={
     *      {"name"="_bearer", "dataType"="string", "required"=true, "description"="Valid jwt token to pass through the app firewall. If not set as a parameter, must be set in the header with 'Bearer ' as a prefix"}
     *  }
     * )
     */
    public function postRemainderEmailAction()
    {
        $process = new Process($this->getConsolePath() . EmailReminderCommand::SCHEDULE_SEND_REMAINDER_COMMAND);
        $process->run();

        if (!$process->isSuccessful()) {
            throw new InternalErrorException('There was an unexpected error while executing the remainder command.');
        }

        return $this->handleView($this->view(null, Response::HTTP_OK));
    }

    /**
     * @Rest\Post("schedule")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws InternalErrorException
     * @ApiDoc(
     *  description="Updates the emails that are going to receive the schedule email.",
     *  section="Task",
     *  statusCodes={
     *         200="Returned when the emails are updated successfully",
     *         500="Returned when there was an error executing the command"
     *     },
     *  parameters={
     *      {"name"="_bearer", "dataType"="string", "required"=true, "description"="Valid jwt token to pass through the app firewall. If not set as a parameter, must be set in the header with 'Bearer ' as a prefix"},
     *      {"name"="admin_only", "dataType"="boolean", "required"=false, "description"="Specify if the schedule email will be sent to the whole team or just the users with the admin role"}
     *  }
     * )
     */
    public function postScheduleEmailAction(Request $request)
    {
        $admin_only = $request->get('admin_only', false);
        $command = $this->getConsolePath() . SendScheduleCommand::SCHEDULE_SEND_EMAIL_COMMAND;
        if($admin_only) {
            $command .= ' --just-admin';
        }

        $process = new Process($command);
        $process->run();

        if (!$process->isSuccessful()) {
            throw new InternalErrorException('There was an unexpected error while executing the remainder command.');
        }

        return $this->handleView($this->view(null, Response::HTTP_OK));
    }

    protected function getConsolePath() {
        return $console = 'php ' . $this->getParameter('kernel.root_dir') . '/../bin/console ';
    }
}
