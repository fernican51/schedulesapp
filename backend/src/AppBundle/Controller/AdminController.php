<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Yaml\Dumper;

class AdminController extends FOSRestController
{
    const SCHEDULES_CONFIG_DIR = '/config/schedule.yml';

    /**
     * @Rest\Put("email")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @ApiDoc(
     *  description="Updates the emails that are going to receive the schedule email.",
     *  section="Admin",
     *  statusCodes={
     *         204="Returned when the emails are updated successfully",
     *         400="Returned when there is invalid data sent",
     *         403="Returned when the user does not have permission to perform this action"
     *     },
     *  parameters={
     *      {"name"="emails", "dataType"="array", "required"=true, "description"="An array of string containing email addresses"}
     *  }
     * )
     */
    public function putAdminEmailAction(Request $request)
    {
        $emailList = $request->get('emails');
        if (!$this->validateEmailList($emailList)) {
            throw new BadRequestHttpException('There is an invalid email on the list sent');
        }

        $validEmailList = array_unique($emailList);

        $kernelDir = $this->getParameter('kernel.root_dir');
        $dumper = new Dumper();
        $config = array('admin_emails' => $validEmailList);
        $yaml = $dumper->dump($config);
        file_put_contents($kernelDir . $this::SCHEDULES_CONFIG_DIR, $yaml);

        return $this->handleView($this->view(null, Response::HTTP_NO_CONTENT));
    }

    protected function validateEmailList($emailList) {
        if(!$emailList || !is_array($emailList) || !count($emailList)) {
            return false;
        }

        $validator = $this->get('validator');
        $constraints = array(
            new Email(),
            new NotBlank()
        );
        foreach ($emailList as $email) {
            $error = $validator->validate($email, $constraints);
            if (count($error)) {
                return false;
            }
        }
        return true;
    }
}
