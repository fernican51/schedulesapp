<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Notification;
use AppBundle\Entity\Schedule;
use AppBundle\Form\ScheduleType;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class ScheduleController extends FOSRestController
{
    /**
     * Validates and saves an schedule entry
     *
     * @param  Schedule $schedule
     * @param  Request $request
     * @param  bool $new is new object
     * @return \Symfony\Component\HttpFoundation\Response
     */
    private function processForm(Schedule $schedule, Request $request, $new = false)
    {
        $statusCode = $new ? Response::HTTP_CREATED : Response::HTTP_NO_CONTENT;
        $form = $this->createForm(ScheduleType::class, $schedule);
        $data = $request->request->all();
        $children = $form->all();
        $toBind = array_intersect_key($data, $children);

        if($new)
        {
            $toBind['user'] = $this->getUser()->getId();
        }

        $form->submit($toBind);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($schedule);
            $em->flush();
            return $this->handleView($this->view($new ? $schedule : null, $statusCode));
        }
        return $this->handleView($this->view($form, Response::HTTP_BAD_REQUEST));
    }

    /**
     * @Rest\Get("schedule")
     * @return \Symfony\Component\HttpFoundation\Response
     * @ApiDoc(
     *  description="Return the schedule list.",
     *  section="Schedule",
     *  statusCodes={
     *         200="Returned when successful",
     *         500="Returned when a non expected error happened getting the list"
     *     }
     * )
     */
    public function getScheduleListAction()
    {
        $schedules = $this->getDoctrine()->getRepository('AppBundle:Schedule')->findCurrentSchedule();

        return $this->handleView($this->view(array('list' => $schedules)));
    }

    /**
     * @Rest\Get("schedule/{id}")
     * @param Schedule $schedule
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @ApiDoc(
     *  description="Return an schedule by id.",
     *  section="Schedule",
     *  statusCodes={
     *         200="Returned when successful",
     *         500="Returned when a non expected error happened getting the schedule"
     *     },
     *  parameters={
     *      {"name"="id", "dataType"="integer", "required"=true, "description"="Schedule id"}
     *  }
     * )
     */
    public function getScheduleAction(Schedule $schedule)
    {
        return $this->handleView($this->view(array('schedule' => $schedule)));
    }

    /**
     * @Rest\Delete("schedule/{id}")
     * @param  Schedule $schedule
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @ApiDoc(
     *  description="Deletes an schedule by it's id.",
     *  section="Schedule",
     *  statusCodes={
     *         204="Returned when an item was deleted successfully",
     *         404="Returned when there is no item with the id passed"
     *     },
     *  parameters={
     *      {"name"="id", "dataType"="integer", "required"=true, "description"="Schedule id"}
     *  }
     * )
     */
    public function deleteScheduleAction(Schedule $schedule)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($schedule);
        $em->flush();
        return $this->handleView($this->view(null, Response::HTTP_NO_CONTENT));
    }

    /**
     * @Rest\Post("schedule")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @ApiDoc(
     *  description="Schedule where it'll be working the current day.",
     *  section="Schedule",
     *  statusCodes={
     *         201="Returned when an item was created successfully",
     *         400="Returned when there is invalid data sent or the user already sent his schedule"
     *     },
     *  parameters={
     *      {"name"="notification_id", "dataType"="integer", "required"=true, "description"="The notification type id for the schedule entry"}
     *  }
     * )
     */
    public function postScheduleAction(Request $request)
    {
        $userRepository = $this->getDoctrine()->getRepository('AppBundle:User');
        if($userRepository->userSentSchedule($this->getUser())) {
            throw new BadRequestHttpException('You already sent your schedule for today.');
        }
        return $this->processForm(new Schedule(), $request, true);
    }

    /**
     * @Rest\Put("schedule/{id}")
     * @param Request $request
     * @param Schedule $schedule
     * @return \Symfony\Component\HttpFoundation\Response
     * @ApiDoc(
     *  description="Updates an schedule notification type by it's id.",
     *  section="Schedule",
     *  statusCodes={
     *         204="Returned when an item was updated successfully",
     *         400="Returned when there is invalid data sent"
     *     },
     *  parameters={
     *      {"name"="notification_id", "dataType"="integer", "required"=true, "description"="The notification type id for the schedule entry"}
     *  }
     * )
     */
    public function putScheduleAction(Request $request, Schedule $schedule)
    {
        return $this->processForm($schedule, $request, true);
    }
}
