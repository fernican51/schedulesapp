<?php
/**
 * Created by PhpStorm.
 * User: fernandofigueroa
 * Date: 4/28/16
 * Time: 4:20 PM
 */

namespace AppBundle\Command;


use AppBundle\Controller\AdminController;
use AppBundle\Entity\Notification;
use AppBundle\Entity\Schedule;
use AppBundle\Entity\User;
use AppBundle\Util\Util;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Parser;

class SendScheduleCommand extends ContainerAwareCommand
{
    const SCHEDULE_SEND_EMAIL_COMMAND = 'schedule:email:send';

    protected function configure()
    {
        $this
            ->setName($this::SCHEDULE_SEND_EMAIL_COMMAND)
            ->setDescription('Sends the schedule for the current day. It can be set to only send the email to the admin.')
            ->addOption(
                'just-admin',
                null,
                InputOption::VALUE_NONE,
                'If set, the task will yell in uppercase letters'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if(Util::isTodayWeekend()) {
           return;
        }

        $onlyToAdmin = $input->getOption('just-admin');
        $doctrine = $this->getContainer()->get('doctrine');
        $scheduleRepository = $doctrine->getRepository('AppBundle:Schedule');
        if($onlyToAdmin) {
            $userRepository = $doctrine->getRepository('AppBundle:User');
            $users = $userRepository->getAdminUsers();
            $recipients = (array_map(function(User $user){
                return $user->getEmail();
            }, $users));
        }
        else
        {
            $kernelDir = $this->getContainer()->getParameter('kernel.root_dir');
            $yaml = new Parser();
            $config = $yaml->parse(file_get_contents($kernelDir . AdminController::SCHEDULES_CONFIG_DIR));
            $recipients = $config['admin_emails'];
        }

        $this->sendSchedule($scheduleRepository->findCurrentSchedule(), $recipients);
    }

    private function sendSchedule($schedules, $recipients) {
        $message = \Swift_Message::newInstance()
            ->setSubject('Daily Status Digest')
            ->setFrom('schedules@admios-sa.com')
            ->setTo($recipients)
            ->setBody($this->getContainer()->get('templating')->render(
                ':Email:schedule.html.twig',
                array(
                    'site_url' => $this->getContainer()->getParameter('site_url'),
                    'schedules' => $this->formatScheduleData($schedules)
                )
            ),
                'text/html');

        $this->getContainer()->get('mailer')->send($message);
    }

    protected function formatScheduleData($schedules) {
        $formattedData = array();
        /**
         * @var Schedule $schedule
         */
        foreach ($schedules as $schedule) {
            /**
             * @var Notification $notification
             */
            $notification =  $schedule->getNotification();
            $notificationId = $schedule->getNotification()->getId();
            if(isset($formattedData[$notificationId])) {
                $formattedData[$notificationId]['users'][] = $schedule->getUser();
            }
            else
            {
                $formattedData[$notificationId] = array(
                    'name' => '(' . strtoupper($notification->getShortDescription()) . ') - ' . $notification->getName(),
                    'users' => array($schedule->getUser())
                );
            }
        }
        return count($formattedData) ? array_values($formattedData) : $formattedData;
    }
}