<?php

namespace AppBundle\Command;

use AppBundle\Entity\User;
use AppBundle\Util\Util;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class EmailReminderCommand extends ContainerAwareCommand
{
    const SCHEDULE_SEND_REMAINDER_COMMAND = 'schedule:email:remainder';

    protected function configure()
    {
        $this
            ->setName($this::SCHEDULE_SEND_REMAINDER_COMMAND)
            ->setDescription('Sends an email reminder to users who did not send the working schedule before the deadline.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if(Util::isTodayWeekend()) {
            return;
        }

        $doctrine = $this->getContainer()->get('doctrine');
        $userRepository = $doctrine->getRepository('AppBundle:User');
        $this->sendReminderMessage($userRepository->findAllReminderAvailable());
    }

    private function sendReminderMessage($needReminderUsers) {

        if(count($needReminderUsers)) {
            $emails = [];
            /**
             * @var User $user
             */
            foreach ($needReminderUsers as $user) {
                $emails[] = $user->getEmail();
            }
            $message = \Swift_Message::newInstance()
                ->setSubject('Daily Status Digest Remainder')
                ->setFrom('schedules@admios-sa.com')
                ->setTo($emails)
                ->setBody($this->getContainer()->get('templating')->render(
                    ':Email:remainder.html.twig',
                    array('site_url' => $this->getContainer()->getParameter('site_url'))
                ),
                    'text/html');

            $this->getContainer()->get('mailer')->send($message);
        }
    }
}