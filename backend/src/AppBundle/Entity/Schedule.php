<?php

namespace AppBundle\Entity;


use AppBundle\Util\Util;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Accessor;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ScheduleRepository")
 * @ORM\Table(name="schedules")
 */
class Schedule
{
    use ORMBehaviors\Timestampable\Timestampable;
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="schedules")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="SET NULL")
     * @Assert\Type(type="AppBundle\Entity\User")
     * @Assert\Valid()
     * @Assert\NotBlank()
     * @Accessor(getter="getUser",setter="setUser")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="Notification", inversedBy="schedules")
     * @ORM\JoinColumn(name="notification_id", referencedColumnName="id", onDelete="SET NULL")
     * @Assert\Type(type="AppBundle\Entity\Notification")
     * @Assert\Valid()
     * @Assert\NotBlank()
     * @Accessor(getter="getNotification",setter="setNotification")
     */
    private $notification;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Schedule
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        if($this->user) {
            return $this->user;
        }

        return $this->id ? Util::getDefaultUser() : null;
    }

    /**
     * Set notification
     *
     * @param \AppBundle\Entity\Notification $notification
     *
     * @return Schedule
     */
    public function setNotification(Notification $notification = null)
    {
        $this->notification = $notification;

        return $this;
    }

    /**
     * Get notification
     *
     * @return \AppBundle\Entity\Notification
     */
    public function getNotification()
    {
        if($this->notification) {
            return $this->notification;
        }

        return $this->id ? Util::getDefaultNotification() : null;
    }
}
