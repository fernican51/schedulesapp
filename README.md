# Schedules App

Small app to register and notify team members office attendance.
  - Symfony 3 API (json and xml).
  - React + Redux.

### Backend Requirements:
* [PHP] > 5.4
* [Composer]
* MySQL Database

### Backend Installation:
Clone the repo and move to the backend folder:
```sh
$ git clone https://fernican51@bitbucket.org/fernican51/schedulesapp.git
$ cd backend
```
Create a jwt directory under var and set a passphrase for ssl:
```sh
$ mkdir -p app/var/jwt
$ openssl genrsa -out var/jwt/private.pem -aes256 4096
$ openssl rsa -pubout -in var/jwt/private.pem -out app/var/jwt/public.pem
```
Enter the info requested by the prompt of the command and configure your host with. Set up your virtualhost to point to the web folder of the backend dir.

   [PHP]: <https://secure.php.net/>
   [Composer]: <https://getcomposer.org/>